import praw
import requests
import json
import re
from furl import furl
from age_gender import find_age_gender
import urllib.request
from tinydb import TinyDB, Query
from difflib import SequenceMatcher
import aniso8601
import datetime
import statistics

with open('places5.json') as f:
    places = json.load(f)

image_dir = '/Volumes/Untitled/'
db = TinyDB('db.json')

def find_youtube_channel_details(id, author):
    #print 'searching for id ', id
    video_response = requests.request("GET", "https://www.googleapis.com/youtube/v3/videos", params={
        "part": "snippet",
        "id": id,
        "key": "AIzaSyAdzKTPhFCdREORoD1NHB4y2t-nMDj_9sk"
        })
    #print video_response.text
    video_details_items = json.loads(video_response.text)['items']
    if len(video_details_items) > 0:
        video_details = video_details_items[0]['snippet']
        channel_id = video_details['channelId']

        User = Query()
        list = db.search(User.id == channel_id)
        if len(list) == 0:
            try:
                video_thumbnail_link = video_details['thumbnails']['standard']['url']
            except:
                video_thumbnail_link = video_details['thumbnails']['default']['url']
            if 'tags' in video_details:
                video_tags = video_details['tags']
            else:
                video_tags = []
            video_category = video_details['categoryId']
            #print 'channel_id', channel_id

            channel_response = requests.request("GET", "https://www.googleapis.com/youtube/v3/channels", params={
                "part": "snippet",
                "id": channel_id,
                "key": "AIzaSyAdzKTPhFCdREORoD1NHB4y2t-nMDj_9sk"
                })

            channel_details = json.loads(channel_response.text)['items'][0]['snippet']
            #print 'name', channel_details['title']
            #print 'description', channel_details['description']
            #print 'thumbnail', channel_details['thumbnails']['high']['url']
            link = channel_details['thumbnails']['high']['url']
            filename = link.split('/')[-1] + '.png'
            urllib.request.urlretrieve(link, image_dir + 'images/' + filename)

            country = ''
            try:
                country = channel_details['country']
            except:
                print("Couldn't find country")

            res = find_age_gender(image_dir + 'images/' + filename)

            demographics = []
            try:
                predicted_ages, predicted_genders, length = res
                for i in range(length):
                    gender = "F" if predicted_genders[i][0] > 0.5 else "M"
                    demographics.append({
                        'gender': gender,
                        'age': int(predicted_ages[i])
                    })
            except:
                print("Couldn't find any faces in the channel profile picture, checking in video thumbnail...")
                try:
                    video_filename = video_thumbnail_link.split('/')[-1]
                    urllib.request.urlretrieve(video_thumbnail_link, image_dir + 'images/' + video_filename)
                    res = find_age_gender(image_dir + 'images/' + video_filename)
                    predicted_ages, predicted_genders, length = res
                    for i in range(length):
                        gender = "F" if predicted_genders[i][0] > 0.5 else "M"
                        demographics.append({
                            'gender': gender,
                            'age': int(predicted_ages[i])
                        })
                except Exception as e:
                    print("Couldn't find any faces for " + channel_id)


            search_response = requests.request("GET", "https://www.googleapis.com/youtube/v3/search", params={
                "part": "snippet",
                "channelId": channel_id,
                "key": "AIzaSyAdzKTPhFCdREORoD1NHB4y2t-nMDj_9sk",
                "maxResults": "50"
                })

            video_ids = []
            locations = []
            search_items = json.loads(search_response.text)['items']
            for item in search_items:
                search_id = item['id']
                try:
                    video_ids.append(search_id['videoId'])
                except Exception:
                    pass
                search_details = item['snippet']
                search_title = search_details['title']
                search_description = search_details['description']
                search_str = search_title.lower() + " " + search_description.lower()
                regex = re.compile('[^a-zA-Z ]')
                plain_str = regex.sub('', search_str)
                plain_words = plain_str.split(" ")
                for word in plain_words:
                    for place in places:
                        if SequenceMatcher(None, word, place).ratio() > 0.95:
                            if not any(place in s for s in locations):
                                locations.append(place)

            all_videos_response = requests.request("GET", "https://www.googleapis.com/youtube/v3/videos", params={
                "part": "contentDetails",
                "id": ",".join(video_ids),
                "key": "AIzaSyAdzKTPhFCdREORoD1NHB4y2t-nMDj_9sk"
                })

            all_videos_items = json.loads(all_videos_response.text)['items']
            sum = datetime.timedelta(milliseconds=0)
            durations = []
            all_videos_count = 0
            for item in all_videos_items:
                all_videos_details = item['contentDetails']
                all_videos_duration = all_videos_details['duration']
                aniso8601_duration = aniso8601.parse_duration(all_videos_duration)
                sum += aniso8601_duration
                durations.append(aniso8601_duration.total_seconds())
                all_videos_count += 1
            avg_duration = (sum/all_videos_count).total_seconds()
            if len(durations) >= 2:
                std_dev = statistics.stdev(durations)
            else:
                std_dev = 0

            return {
                'video_id': id,
                'video_tags': video_tags,
                'video_category': video_category,
                'id': channel_id,
                'name': channel_details['title'],
                'description': channel_details['description'],
                'thumbnail': filename,
                'country': country,
                'demographics': demographics,
                'reddit_username': author,
                'locations': locations,
                'avg_duration': avg_duration,
                'std_dev_duration': std_dev,
                'min_duration': min(durations),
                'max_duration': max(durations)
            }
        else:
            return None
    else:
        return None

def parse_reddit(n):
    reddit = praw.Reddit(client_id='6gijqNlK3OA1VQ',
                         client_secret='l_1a6Ai0F9lOrBiF_6Kx9A15sxo',
                         user_agent='PRAW:PRAW_testing:v0.0.1 (by /u/oppai_suika)')

    print(reddit.read_only)

    pattern = re.compile("^(https?\:\/\/)?(www\.youtube\.com|youtu\.?be)\/.+$")
    small_pattern = re.compile("^(https?\:\/\/)?(youtu\.?be)\/.+$")
    for submission in reddit.subreddit('vlog').new(limit=n):
        #print submission.title, submission.url)
        author = submission.author.name
        if bool(pattern.match(submission.url)):
            f = furl(submission.url)
            #print submission.title, submission.url, f.args
            token = ''
            if 'v' in f.args:
                token = f.args['v']
            elif 'u' in f.args:
                fu = furl(f.args['u'])
                token = fu.args['v']
            elif bool(small_pattern.match(submission.url)):
                url_list = submission.url.split('/')
                token = url_list[3]
            if len(token) == 11:
                User = Query()
                list = db.search(User.video_id == token)
                if len(list) == 0:
                    print(submission.title, token)
                    details = find_youtube_channel_details(token, author)
                    if details != None:
                        db.insert(details)
        #submission.link_flair_text

parse_reddit(200)
'''res = find_age_gender('test.jpg')
predicted_ages, predicted_genders, length = res
for i in range(length):
    print(int(predicted_ages[i]), "F" if predicted_genders[i][0] > 0.5 else "M")'''
