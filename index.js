var fs = require('fs')
var ProgressBar = require('progress');
var request = require('request');
var path = require('path')
var Box = require("cli-box");
var drawing = require('pngjs-draw');
var png = drawing(require('pngjs').PNG);
var PNGImage = require('pngjs-image');
const chalk = require('chalk');

var box_width = 45;

console.log(Box(box_width + "x2", chalk.cyan('Bulk download images from Reddit\n') + "by u/oppai_suika").toString());
getMemeData('NewTubers')

function getMemeData(subreddit) {
  console.log(Box(box_width + "x2", chalk.yellow('Downloading images...\n') + "from r/" + subreddit).toString());
  if (!fs.existsSync(subreddit)){
    fs.mkdirSync(subreddit);
  }
  if (fs.existsSync('top.json')) {
    console.log('Reading from top.json')
    fs.readFile('top.json', 'utf8', function (err,data) {
      if (err) {
        return console.log(err);
      } else {
        parse_body(data, subreddit)
      }
    });
  } else {
    request('https://www.reddit.com/r/' + subreddit + '/top.json?limit=100', function (error, response, body) {
      console.log("Status code: " + chalk.gray(response && response.statusCode))
      if (error) {
        console.log(error)
      } else {
        parse_body(body, subreddit)
      }
    });
  }
}

function parse_body(body, subreddit) {
  var json = JSON.parse(body)
  var children = json.data.children;
  var bar = new ProgressBar(':bar', {
    width: box_width,
    total: children.length
  });
  for (var i = 0; i < children.length; i++) {
    var meme = children[i].data;
    var array = meme.url.split('/');
    var lastsegment = array[array.length-1];
    download(meme.url, bar, subreddit + "/" + lastsegment, function(){
      bar.tick()
      if (bar.complete) {
        finishedDownloading()
      }
    });
  }
}

var download = function(uri, bar, filename, callback){
  request.head(uri, function(err, res, body) {
    if (res) {
      if ((res.headers['content-type'] == "image/jpeg") || (res.headers['content-type'] == "image/png")) {
        request(uri).pipe(fs.createWriteStream(filename)).on('close', callback);
      } else {
        bar.tick()
        if (bar.complete) {
          finishedDownloading()
        }
      }
    }
  });
};

function finishedDownloading() {
  console.log("Finished downloading")
}
